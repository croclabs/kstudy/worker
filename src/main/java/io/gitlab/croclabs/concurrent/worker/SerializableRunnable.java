package io.gitlab.croclabs.concurrent.worker;

import java.io.Serializable;

public interface SerializableRunnable extends Serializable, Runnable { }
