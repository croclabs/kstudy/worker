package io.gitlab.croclabs.concurrent.worker;

import java.io.Serial;

public class WorkerException extends RuntimeException {
	@Serial
	private static final long serialVersionUID = 7810772173630557754L;

	public WorkerException() {
	}

	public WorkerException(String message) {
		super(message);
	}

	public WorkerException(String message, Throwable cause) {
		super(message, cause);
	}

	public WorkerException(Throwable cause) {
		super(cause);
	}

	public WorkerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
