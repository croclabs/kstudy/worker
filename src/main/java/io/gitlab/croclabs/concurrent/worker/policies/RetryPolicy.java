package io.gitlab.croclabs.concurrent.worker.policies;

import java.util.concurrent.*;

public class RetryPolicy implements RejectedExecutionHandler {
	private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, 5, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
	private final Semaphore semaphore = new Semaphore(0);

	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		if (executor.isTerminating() || executor.isTerminated() || executor.isShutdown()) {
			return;
		}

		threadPoolExecutor.execute(() -> {
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			}

			executor.execute(r);
		});
	}

	public ThreadPoolExecutor getThreadPoolExecutor() {
		return threadPoolExecutor;
	}

	public Semaphore getSemaphore() {
		return semaphore;
	}

	public void release() {
		if (semaphore.hasQueuedThreads()) {
			semaphore.release();
		}
	}
}
