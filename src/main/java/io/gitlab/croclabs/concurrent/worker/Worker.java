package io.gitlab.croclabs.concurrent.worker;

import io.gitlab.croclabs.concurrent.worker.policies.RetryPolicy;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Worker {
	private final Object id;
	private final WorkerExecutor pool;
	private final ConcurrentLinkedQueue<Serializable> tasks = new ConcurrentLinkedQueue<>();

	public Worker(Object id) {
		this(id, new WorkerExecutor());
	}

	public Worker(Object id, int maxThreads) {
		this(id, maxThreads, new RetryPolicy());
	}

	public Worker(Object id, int maxThreads, RejectedExecutionHandler rejectedExecutionHandler) {
		this(id, new WorkerExecutor(maxThreads, rejectedExecutionHandler));
	}

	public Worker(Object id, WorkerExecutor workerExecutor) {
		super();
		pool = workerExecutor;
		this.id = id;

		load();
	}

	public CompletableFuture<Void> add(SerializableRunnable runnable) {
		if (runnable == null || tasks.contains(runnable)) {
			return CompletableFuture.failedFuture(new WorkerException("Runnable already added"));
		}

		tasks.add(runnable);

		return CompletableFuture.runAsync(runnable, getPool()).handleAsync((o, t) -> {
			tasks.remove(runnable);
			return handleAsync(o, t);
		});
	}

	public <T> CompletableFuture<T> add(SerializableCallable<T> callable) {
		if (callable == null || tasks.contains(callable)) {
			return CompletableFuture.failedFuture(new WorkerException("Callable already added"));
		}

		tasks.add(callable);

		return CompletableFuture.supplyAsync(() -> {
			try {
				return callable.call();
			} catch (Exception e) {
				throw new WorkerException(e);
			}
		}, getPool()).handleAsync((o, t) -> {
			tasks.remove(callable);
			return handleAsync(o, t);
		});
	}

	private <T> T handleAsync(T o, Throwable t) {
		if (this.pool.getRejectedExecutionHandler() instanceof RetryPolicy retryPolicy) {
			retryPolicy.release();
		}

		if (t != null) {
			throw new WorkerException(t);
		}

		return o;
	}

	public ThreadPoolExecutor getPool() {
		return pool;
	}

	public Object getId() {
		return id;
	}

	@SuppressWarnings("unchecked")
	private void load() {
		Logger logger = Logger.getLogger(this.getClass().getName());

		Set<Serializable> loaded = new LinkedHashSet<>();
		String fileUri = "./tmp/croclabs/worker/" + this.getId() + ".tasks.tmp";

		File f = new File(fileUri);

		if (!f.exists()) return;

		try (
				FileInputStream fis = new FileInputStream(fileUri);
				ObjectInputStream ois = new ObjectInputStream(fis)
		) {
			loaded = (Set<Serializable>) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			logger.log(Level.SEVERE, "Reading tasks tmp file failed: {0}", sw);
		}

		try {
			Files.deleteIfExists(f.toPath());
		} catch (IOException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			logger.log(Level.SEVERE, "Deleting tasks tmp file failed: {0}", sw);
		}

		loaded.forEach(serializable -> {
			if (serializable instanceof SerializableRunnable serializableRunnable) {
				this.add(serializableRunnable);
			} else if (serializable instanceof SerializableCallable<?> serializableCallable) {
				this.add(serializableCallable);
			}
		});
	}

	public void shutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			Logger logger = Logger.getLogger(this.getClass().getName());

			String fileUri = "./tmp/croclabs/worker/" + this.getId() + ".tasks.tmp";

			getPool().getQueue().drainTo(new ArrayList<>());
			getPool().shutdown();

			try {
				if (getPool().awaitTermination(1, TimeUnit.HOURS)) {
					logger.log(Level.INFO, "Worker shutdown successful");
				} else {
					logger.log(Level.INFO, "Worker shutdown elapsed");
				}
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			}


			Set<Serializable> runnables = new LinkedHashSet<>(tasks);
			logger.log(Level.INFO, "Tasks for this worker: {0}", runnables.size());

			try {
				Files.deleteIfExists(Path.of(fileUri));
			} catch (IOException e) {
				throw new WorkerException("Deleting task tmp file failed", e);
			}

			if (runnables.isEmpty()) return;

			try {
				File f = new File(fileUri);
				Files.createDirectories(f.getParentFile().toPath());
				Files.createFile(f.toPath());
				FileOutputStream fos = new FileOutputStream(fileUri);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(runnables);
				oos.close();
			} catch (IOException e) {
				throw new WorkerException("Writing task tmp file failed", e);
			}
		}));
	}
}
