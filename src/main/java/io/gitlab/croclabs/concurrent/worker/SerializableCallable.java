package io.gitlab.croclabs.concurrent.worker;

import java.io.Serializable;
import java.util.concurrent.Callable;

public interface SerializableCallable<V> extends Callable<V>, Serializable { }
