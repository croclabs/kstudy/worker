package io.gitlab.croclabs.concurrent.worker;

import io.gitlab.croclabs.concurrent.worker.policies.RetryPolicy;

import java.util.concurrent.*;

public class WorkerExecutor extends ThreadPoolExecutor {
	public WorkerExecutor() {
		super(0, 1, 0, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
	}

	public WorkerExecutor(int maxThreads) {
		this(maxThreads, new RetryPolicy());
	}

	public WorkerExecutor(int maxThreads, RejectedExecutionHandler rejectedExecutionHandler) {
		this(new ThreadPoolExecutor(
				0,
				maxThreads,
				5,
				TimeUnit.MINUTES,
				new SynchronousQueue<>(),
				rejectedExecutionHandler
		), TimeUnit.MINUTES, new SynchronousQueue<>(), rejectedExecutionHandler);
	}

	public WorkerExecutor(ThreadPoolExecutor threadPoolExecutor, TimeUnit unit, BlockingQueue<Runnable> queue, RejectedExecutionHandler rejectedExecutionHandler) {
		super(
				threadPoolExecutor.getCorePoolSize(),
				threadPoolExecutor.getMaximumPoolSize(),
				threadPoolExecutor.getKeepAliveTime(unit),
				unit,
				queue,
				rejectedExecutionHandler
		);
	}
}
